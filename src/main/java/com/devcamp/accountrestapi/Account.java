package com.devcamp.accountrestapi;

public class Account {
    private String id;
    private String name;
    private int balance;
    //khởi tạo 2 tham số
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }
    //khởi tạo 3 tham số
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
    //getter 
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getBalance() {
        return balance;
    }
    //toString
    @Override
    public String toString() {
        return "Account [id=" + id + ", name=" + name + ", balance=" + balance + "]";
    }
    public int credit(int amount){
        balance += amount;
        return balance;
    };
    public int debit(int amount){
        if (amount <= balance){
            balance = balance - amount;
            return balance;
        }
        else {
            System.out.println("Amount debit exceed balance");
            return balance;
        }
    };
    public int transferTo(Account target, int amount){
        if (amount <= this.balance){
            //transfer amount to another account
            target.balance = target.balance + amount;
            this.balance =  this.debit(amount);
            return  target.balance;
        } 
        else {
            System.out.println("Amount transfered exceed balance");
            return target.balance;
        }
    }
}
