package com.devcamp.accountrestapi.controllers;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.accountrestapi.Account;

@RestController
@RequestMapping
@CrossOrigin
public class AccountRestApiController {
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
        Account account1 = new Account("100001", "Current Account", 10000000);
        Account account2 = new Account("200001", "Deposit Account 1", 50000000);
        Account account3 = new Account("200002", "Deposit Account 2", 80000000);
        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);
        ArrayList<Account> accounts = new ArrayList<>();
        //thêm các đối tượng vào arraylist
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;
    }
    
    
}
